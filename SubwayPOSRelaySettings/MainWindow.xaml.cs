﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IO = System.IO;

namespace SubwayPOSRelaySettings
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {   //copied from SubwayPOSRelay.SubwayPOSRelay to avoid reference
        private static readonly string targetCacheFilePath = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
                                                                             "EZUniverse", "SubwayPOSRelayForwardAddress.txt");

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (File.Exists(targetCacheFilePath))
            {
                string address = String.Empty;
                string useConfig = String.Empty;
                using (var file = File.OpenText(targetCacheFilePath))
                {
                    string data = file.ReadToEnd();
                    string[] lines = data.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string line in lines)
                    {
                        if (line.StartsWith("IPAdress:"))
                        {
                            address = line.Replace("IPAdress:", "");
                        }
                        else if (line.StartsWith("useOnlyConfigAddress:"))
                        {
                            useConfig = line.Replace("useOnlyConfigAddress:", "");
                        }
                    }

                    //address = file.ReadLine();
                }

                IPAddress parsed;
                if (!String.IsNullOrEmpty(address) && IPAddress.TryParse(address, out parsed)) //check if valid ip address
                {
                    tbAddress.Text = parsed.ToString();
                }

                bool useOnlyConfigAddress;
                if (!bool.TryParse(useConfig, out useOnlyConfigAddress))
                {
                    useOnlyConfigAddress = false;
                }

                cbOverrideAddressFromDC.IsChecked = useOnlyConfigAddress;
            }
        }

        private void bSet_Click(object sender, RoutedEventArgs e)
        {
            var address = tbAddress.Text;
            bool useOnlyConfigAddress = cbOverrideAddressFromDC.IsChecked.HasValue ? cbOverrideAddressFromDC.IsChecked.Value : false;

            IPAddress parsed;
            if (!String.IsNullOrEmpty(address) && IPAddress.TryParse(address, out parsed)) //check if valid ip address
            {
                if (!Directory.Exists(IO.Path.GetDirectoryName(targetCacheFilePath)))
                {
                    Directory.CreateDirectory(IO.Path.GetDirectoryName(targetCacheFilePath));
                }

                using (var file = File.CreateText(targetCacheFilePath))
                {
                    file.WriteLine(String.Format("IPAdress:{0}\r\nuseOnlyConfigAddress:{1}", parsed.ToString(), useOnlyConfigAddress));
                    //file.Write(parsed.ToString());
                }

                if (MessageBox.Show("SubwayPOSRelay service needs to be restarted. Do you want to restart it now?",
                                    "Restart SubwayPOSRelay", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    bSet.IsEnabled = false;
                    lStatus.Content = "Restarting service...";

                    RestartService();

                    lStatus.Content = String.Empty;
                    bSet.IsEnabled = true;
                }
            }
            else
            {
                MessageBox.Show(String.Format("Entered value '{0}' seems to be an invalid IP address.", address),
                                "Invalid value", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void RestartService()
        {
            if (!ServiceController.GetServices().Any(q => "SubwayPOSRelay".Equals(q.ServiceName)))
            {
                MessageBox.Show("SubwayPOSRelay service seems to be missing. Please contact support", "Missing SubwayPOSRelay",
                                MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            var service = new ServiceController("SubwayPOSRelay");
            if (!(service.Status == ServiceControllerStatus.Stopped) &&
                !(service.Status == ServiceControllerStatus.StopPending))
            {
                service.Stop();
                try
                {
                    service.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(30));
                }
                catch (System.ServiceProcess.TimeoutException)
                {
                    MessageBox.Show("Could not stop SubwayPOSRelay service within 30 seconds. Please try to restart it again later.",
                                    "Failed to stop SubwayPOSRelay", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
            }

            service.Start();
            try
            {
                service.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(30));
            }
            catch (System.ServiceProcess.TimeoutException)
            {
                MessageBox.Show("Could not start SubwayPOSRelay service within 30 seconds. Please try to restart it again.",
                                "Failed to start SubwayPOSRelay", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            MessageBox.Show("SubwayPOSRelay service was restarted successfully.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
