﻿namespace SubwayPOSRelay
{
    using Common.Logging;
    using EZProtocolTranslator.Listener;
    using EZProtocolTranslator.Sender;
    using EZWebserviceWrapper;
    using System;
    using System.Collections.Concurrent;
    using System.Configuration;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.Linq;
    using System.Reflection;
    using System.Diagnostics;
    using System.Diagnostics.Eventing.Reader;

    using Microsoft.Win32;
    using TerminalData;
    using Config;

    public partial class SubwayPOSRelay : ServiceBase
    {
        private const string DEFAULT_SERVICE_URI = "EZSales@https://ssdata.ipcoop.com/DataWebservice/ezWebservice.asmx;zip;encrypt;minsize=1024;key=123ezUniverse321";
        private const string DEFAULT_DATA_URI = "contr@https://ssdata.ipcoop.com/DataWebservice/ezWebservice.asmx;zip;encrypt;minsize=1024;key=123ezUniverse321";

        private static readonly ILog logger = LogManager.GetCurrentClassLogger();
        private static readonly string targetCacheFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
                                                                          "EZUniverse", "SubwayPOSRelayForwardAddress.txt");

        private static readonly SubwayPOSRelayConfiguration Config = SubwayPOSRelayConfiguration.Instance;

        private readonly object syncRoot = new object();
        private readonly AutoResetEvent queueReady = new AutoResetEvent(false);
        private readonly AutoResetEvent updaterReady = new AutoResetEvent(false);

        private Listener listener;
        private Sender sender;
        private IPEndPoint target;
        private bool useOnlyConfigAddress = false;

        private int listenPort, forwardPort;
        private int checkAddressIntervalDisconnected = 5 * 60 * 1000;
        private int checkAddressIntervalConnected = 15 * 60 * 1000;
        private int sendAliveInterval = 2 * 60 * 1000;
        private int maxQueueSize;

        private bool checkingForAddressMode = false;
        private Timer checkAddressTimer;
        private Timer sendAliveTimer;
        private Timer deployUpdaterTimer;
        private Timer executeUpdaterTimer;

        private Process updaterProcess;

        private string shutdownQueryPath = "*[System/EventID=1074]";
        private EventLogWatcher shutdownLogWatcher;

        private DCConnector dcConnector;
        private IMessage lastMessage = null;

        private ConcurrentQueue<IMessage> queue = new ConcurrentQueue<IMessage>();
        private Thread senderThread;
        private Thread updaterThread;
        private bool stop = false;

        private bool startupSending = false;
        private bool startupSend = false;

        public SubwayPOSRelay()
        {
            InitializeComponent();

            checkAddressTimer = new Timer(CheckAddress);
            sendAliveTimer = new Timer(SendAlive);
        }

        public void StartService(string[] args)
        {
            OnStart(args);
        }

        string serviceVersion;

        public string ServiceVersion
        {
            get
            {
                if (String.IsNullOrEmpty(serviceVersion))
                {
                    try
                    {
                        serviceVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }

                return serviceVersion;
            }
        }

        protected override void OnStart(string[] args)
        {
            logger.Info("Starting SubwayPOSRelay service");

            senderThread = new Thread(SenderThread);
            senderThread.Start();
            logger.Debug("Starting sender thread");

            updaterThread = new Thread(UpdaterThread);
            updaterThread.Start();
            logger.Debug("Starting updater thread");

            InitConfiguration();
            logger.Debug("Configuration read");

            sender = new TcpSender();

            logger.Debug("Sender prepared");

            checkAddressTimer.Change(checkAddressIntervalConnected, checkAddressIntervalConnected);
            checkingForAddressMode = false;
            logger.Info("Check address timer in connected mode");

            SendStartup();

            listener = new UdpListener(listenPort);
            listener.PacketReceived += Listener_PacketReceived;
            listener.StartListening();
            logger.Debug("Listener prepared");

            InitShutdownWatcher();
            logger.Debug("Shutdown watcher prepared");

            sendAliveTimer.Change(sendAliveInterval, sendAliveInterval);

            logger.Info("SubwayPOSRelay service started");

            logger.Alert(new Alerts.AlertAssemblyVersion(ServiceVersion));
        }

        private void InitShutdownWatcher()
        {
            try
            {
                if (shutdownLogWatcher != null)
                {
                    shutdownLogWatcher.Enabled = false;
                    shutdownLogWatcher.Dispose();
                    shutdownLogWatcher = null;
                }

                shutdownLogWatcher = new EventLogWatcher(new EventLogQuery("System", PathType.LogName, shutdownQueryPath));
                shutdownLogWatcher.EventRecordWritten += new EventHandler<EventRecordWrittenEventArgs>(shutdownLogWatcher_EventRecordWritten);
                shutdownLogWatcher.Enabled = true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        void shutdownLogWatcher_EventRecordWritten(object sender, EventRecordWrittenEventArgs e)
        {
            try
            {
                if (e.EventRecord != null)
                {
                    SendShutdown(e.EventRecord.FormatDescription());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private string GetLastShutdownMessage()
        {
            string message = "";
            string queryPath = "*[System/EventID=6008 or System/EventID=1074]";
            EventLogQuery eventsQuery = new EventLogQuery("System", PathType.LogName, queryPath);
            try
            {
                EventLogReader logReader = new EventLogReader(eventsQuery);
                logReader.Seek(SeekOrigin.End, 0);
                EventRecord evRec = logReader.ReadEvent();
                if (evRec != null)
                {
                    message = evRec.FormatDescription();
                }
                else
                {
                    message = "No information about shutdown in System event log";
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return message;
        }

        private void InitConfiguration()
        {
            if (!Int32.TryParse(ConfigurationManager.AppSettings["listenPort"], out listenPort))
                listenPort = 7788;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["forwardPort"], out forwardPort))
                forwardPort = 7789;

            if (!Int32.TryParse(ConfigurationManager.AppSettings["maxQueueSize"], out maxQueueSize))
                maxQueueSize = 10000;

            int minutes;
            if (Int32.TryParse(ConfigurationManager.AppSettings["checkAddressIntervalDisconnectedMinutes"], out minutes))
                checkAddressIntervalDisconnected = GetInterval(minutes);
            if (Int32.TryParse(ConfigurationManager.AppSettings["checkAddressIntervalConnectedMinutes"], out minutes))
                checkAddressIntervalConnected = GetInterval(minutes);
            if (Int32.TryParse(ConfigurationManager.AppSettings["sendAliveIntervalMinutes"], out minutes))
                sendAliveInterval = GetInterval(minutes);

            if (File.Exists(targetCacheFilePath))
            {
                try
                {
                    string address = String.Empty;
                    string useConfig = String.Empty;
                    using (var file = File.OpenText(targetCacheFilePath))
                    {
                        string data = file.ReadToEnd();
                        string[] lines = data.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string line in lines)
                        {
                            if (line.StartsWith("IPAdress:"))
                            {
                                address = line.Replace("IPAdress:", "");
                            }
                            else if (line.StartsWith("useOnlyConfigAddress:"))
                            {
                                useConfig = line.Replace("useOnlyConfigAddress:", "");
                            }
                        }
                    }

                    IPAddress targetIP;
                    if (IPAddress.TryParse(address, out targetIP))
                    {
                        target = new IPEndPoint(targetIP, forwardPort);
                    }

                    if (!bool.TryParse(useConfig, out useOnlyConfigAddress))
                    {
                        useOnlyConfigAddress = false;
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn(ex);
                }
            }

            string serviceUri = ConfigurationManager.AppSettings["serviceUri"] ?? DEFAULT_SERVICE_URI;
            string dataUri = ConfigurationManager.AppSettings["dataUri"] ?? DEFAULT_DATA_URI;

            if (!String.IsNullOrWhiteSpace(serviceUri) && !String.IsNullOrWhiteSpace(dataUri))
            {
                dcConnector = new DCConnector(dataUri, serviceUri);
            }
            else
            {
                logger.Warn("serviceUri is missing");
                dcConnector = null;
            }
        }

        private int GetInterval(int minutes)
        {
            return minutes > 0 ? minutes * 60 * 1000 : Timeout.Infinite;
        }

        protected override void OnStop()
        {
            logger.Info("SubwayPOSRelay service stoppring started");
            stop = true;

            lock (syncRoot)
            {
                if (shutdownLogWatcher != null)
                {
                    shutdownLogWatcher.Enabled = false;
                    shutdownLogWatcher.Dispose();
                    shutdownLogWatcher = null;
                }
                logger.Debug("Shutdown watcher disposed");

                if (sendAliveTimer != null)
                {
                    sendAliveTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    sendAliveTimer.Dispose();
                    sendAliveTimer = null;
                }
                logger.Debug("Alive timer disposed");

                if (checkAddressTimer != null)
                {
                    checkAddressTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    checkAddressTimer.Dispose();
                    checkAddressTimer = null;
                }
                logger.Debug("CheckAddress timer disposed");

                if (listener != null)
                {
                    listener.Dispose();
                    listener = null;
                }
                logger.Debug("Listener disposed");

                if (sender != null)
                {
                    sender.Dispose();
                    sender = null;
                }
                logger.Debug("Sender disposed");

                if (senderThread != null)
                {
                    queueReady.Set();
                    if (!senderThread.Join(30 * 1000))
                    {
                        logger.Warn("Failed to stop sender thread");
                        senderThread.Abort();
                    }
                    senderThread = null;
                }
                logger.Debug("Sender thread disposed");

                if (updaterThread != null)
                {
                    updaterReady.Set();
                    if (!updaterThread.Join(30 * 1000))
                    {
                        logger.Warn("Failed to stop updater thread");
                        updaterThread.Abort();
                    }
                    updaterThread = null;
                }
                logger.Debug("Updater thread disposed");

                queueReady.Dispose();
                logger.Debug("Reset event disposed");
            }

            logger.Info("SubwayPOSRelay service stopped");
        }

        private void Listener_PacketReceived(byte[] message, IPEndPoint ep)
        {
            logger.Debug("Message received");
            if (!POSData.IsValidMessage(message, ep))
                return;

            var msg = new Message(message, ep);
            lastMessage = msg;

            Config.UpdateTerminalInfo((new TerminalDataAdapter(msg)).Terminal);

            queue.Enqueue(msg);
            queueReady.Set();
            lock (syncRoot)
            {
                while (queue.Count > maxQueueSize)
                {
                    logger.InfoFormat("Removing overflowing ({0}) message", queue.Count);
                    IMessage dummy;
                    if (!queue.TryDequeue(out dummy))   //remove oldest messages
                    {
                        break;
                    }
                }
            }
        }

        private void CheckAddress(object state)
        {
            logger.Debug("Checking for address started");
            if (lastMessage == null)
            {
                logger.Warn("Could not check controller address due to lack of received message");
                return;
            }

            var temp = GetTargetAddress(lastMessage);
            if (temp != null)
            {
                target = temp;
                queueReady.Set();
            }
            logger.Debug("Checking for address finished");
        }

        private void SendAlive(object state)
        {
            logger.Debug("Sending alive");
            if (target == null)
            {
                logger.Warn("Could not send alive to controller due to lack of controller IP address");
                return;
            }

            try
            {
                string alive = new POSData(lastMessage).GetAliveMessage(target);
                var bytes = Encoding.ASCII.GetBytes(alive);
                if (!TrySend(target, bytes))
                {
                    logger.WarnFormat("Could not send 'alive' message to '{0}'", target);
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }

            logger.Alert(new Alerts.AlertAlive());
        }

        private void SendStartup()
        {
            if (startupSending)
            {
                logger.Debug("Startup info is being send");
            }
            else
            {
                startupSending = true;
                logger.Debug("Sending startup");
                if (startupSend)
                {
                    logger.Debug("Startup info already send");
                }
                else
                {
                    string startupMessage = GetLastShutdownMessage();
                    try
                    {
                        string alive = new POSData(lastMessage).GetSpecialMessage(target, startupMessage, POSData.SpecialMessageType.Startup);
                        var bytes = Encoding.ASCII.GetBytes(alive);
                        if (!TrySend(target, bytes))
                        {
                            logger.WarnFormat("Could not send 'startup' message to '{0}'", target);
                        }
                        else
                        {
                            startupSend = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Warn(ex);
                    }
                }
                startupSending = false;
            }
        }

        private void SendShutdown(string shutdownMessage)
        {
            logger.Debug("Sending shutdown");
            try
            {
                string alive = new POSData(lastMessage).GetSpecialMessage(target, shutdownMessage, POSData.SpecialMessageType.SystemShutdown);
                var bytes = Encoding.ASCII.GetBytes(alive);
                if (!TrySend(target, bytes))
                {
                    logger.WarnFormat("Could not send 'shutdown' message to '{0}'", target);
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }
        }

        bool localSend = true;

        private bool SendMessage(IMessage message)
        {
            if (localSend)
            {
                return TrySend(target, message.Bytes);
            }
            else
            {
                return dcConnector.SendMessage(message);
            }
        }

        private IPEndPoint GetTargetAddress(IMessage message)
        {
            if (dcConnector == null || lastMessage == null)
            {
                return null;
            }

            var controllerIp = dcConnector.GetControllerAddress(new RelayState(lastMessage, queue.Count()));
            if (controllerIp == null)
            {
                return null;
            }

            if (useOnlyConfigAddress)
            {
                logger.Info("Configuration option useOnlyConfigAddress is set which prevents using received address from DC");
                return null;
            }

            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(targetCacheFilePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(targetCacheFilePath));
                }

                using (var file = File.CreateText(targetCacheFilePath))
                {
                    file.WriteLine(String.Format("IPAdress:{0}\r\nuseOnlyConfigAddress:{1}", controllerIp, useOnlyConfigAddress));
                }
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }

            return new IPEndPoint(controllerIp, forwardPort);
        }

        private bool TrySend(IPEndPoint target, byte[] message)
        {
            if (target == null)
            {
                logger.Warn("Target is not defined");
                return false;
            }
            try
            {
                if (sender.ConnectAndSend(target, message))
                {
                    SendSuccesfull();
                    return true;
                }
                else
                {
                    SendFailure();
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Alert(new Alerts.AlertError(String.Format("Erorr during send: {0}", ex.Message)));
                SendFailure();
                return false;
            }
        }

        private void SendFailure()
        {
            lock (syncRoot)
            {
                localSend = false;
                // put check address timer into not connected state, if not put already
                if (!checkingForAddressMode)
                {
                    if (checkAddressTimer != null && lastMessage != null)
                    {
                        checkAddressTimer.Change(0, checkAddressIntervalDisconnected);
                        checkingForAddressMode = true;
                        logger.InfoFormat("Checking address timer in disconnected interval");
                    }
                }
            }
        }

        private void SendSuccesfull()
        {
            lock (syncRoot)
            {
                localSend = true;
                // put check address timer into connected state, if not put already
                if (checkingForAddressMode)
                {
                    if (checkAddressTimer != null)
                    {
                        checkAddressTimer.Change(checkAddressIntervalConnected, checkAddressIntervalConnected);
                        checkingForAddressMode = false;
                        logger.Info("Check address timer in connected mode");
                    }
                }

                // send startup if not send
                if (!startupSend && !startupSending)
                {
                    SendStartup();
                }

                // unblock waiting to send other messages
                queueReady.Set();
            }
        }

        private void SenderThread()
        {
            try
            {
                while (!stop)
                {
                    IMessage message;
                    if (queue.TryDequeue(out message))
                    {
                        bool sent = false;
                        while (!sent && !stop)
                        {
                            sent = SendMessage(message);

                            if (!sent)
                            {
                                logger.Info("Message could not be sent, wait one minute");
                                queueReady.WaitOne(60 * 1000); // wait one minute with sending this message
                            }
                        }
                    }
                    else
                    {
                        // when nothing in queue or connection is not available, event is reset and thread will wait, otherwise, thread will go further
                        logger.Debug("Wait until new message is received");
                        queueReady.WaitOne();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                if (!stop)
                {
                    lock (syncRoot)
                    {
                        if (!stop)
                        {
                            senderThread = new Thread(SenderThread);
                            senderThread.Start();
                        }
                    }
                }
            }
        }

        private void UpdaterThread()
        {
            try
            {
                while (!stop)
                {
                    // check time
                    var now = DateTime.Now;
                    
                    //TimeSpan nn = new TimeSpan(
                    //if ((now.Minute == 7) && Config.UpdaterExecuteHours.Any(i => { return i == now.Hour; }))
                    if (Config.UpdaterExecuteTime.Any(i => { return ((now.Hour == i.Hours) && (now.Minute == i.Minutes)); }))
                    {
                        // run updater
                        StartUpdater(20 * 1000);
                    }
                    logger.Debug("updaterThread sleep for 1 minute");
                    updaterReady.WaitOne(60 * 1000);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                if (!stop)
                {
                    lock (syncRoot)
                    {
                        if (!stop)
                        {
                            updaterThread = new Thread(UpdaterThread);
                            updaterThread.Start();
                        }
                    }
                }
            }
        }

        private void StartUpdater(int delay)
        {
            logger.InfoFormat("Updater deployment scheduled in {0} sec.", delay / 1000);

            try
            {
                if (Config.UniversalUpdaterNode == null)
                {
                    logger.Warn(m => m("Update configuration not found"));
                }
                else
                {
                    deployUpdaterTimer = new Timer(DeployUpdater);
                    deployUpdaterTimer.Change(delay, Timeout.Infinite);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        private void DeployUpdater(object state)
        {
            try
            {
                if (deployUpdaterTimer != null)
                {
                    deployUpdaterTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    deployUpdaterTimer.Dispose();
                    deployUpdaterTimer = null;
                }

                // check if update process is still running - kill
                CheckUpdater();

                if (!Directory.Exists(Config.UpdaterFolder))
                    Directory.CreateDirectory(Config.UpdaterFolder);


                var universalUpdaterPath = Path.Combine(Config.UpdaterFolder, "EZUniversalUpdaterApp.exe");
                var universalUpdaterConfigPath = String.Format("{0}.config", universalUpdaterPath);
                var servicesConfigPath = Path.Combine(Config.UpdaterFolder, "services.xml");

                ResourceExtractor.ExtractResourceToFile("SubwayPOSRelay.Resources.EZUniversalUpdaterApp.exe", universalUpdaterPath);
                ResourceExtractor.ExtractResourceToFile("SubwayPOSRelay.Resources.EZUniversalUpdaterApp.exe.config", universalUpdaterConfigPath);
                ResourceExtractor.ExtractResourceToFile("SubwayPOSRelay.Resources.services.xml", servicesConfigPath);

                var doc = new XmlDocument();
                doc.Load(universalUpdaterConfigPath);

                var elementToEncrypt = doc.GetElementsByTagName("ezUniversalUpdater")[0] as XmlElement;
                if (elementToEncrypt == null)
                {
                    throw new XmlException(String.Format("The specified element {0} was not found", "ezUniversalUpdater"));
                }

                elementToEncrypt.InnerXml = Config.UniversalUpdaterNode.InnerXml;

                elementToEncrypt.InnerXml = elementToEncrypt.InnerXml
                    .Replace("@@INSTANCEID@@", Config.InstanceID)
                    .Replace("@@STORENUMBER@@", Config.StoreNumber)
                    .Replace("@@SATELLITENUMBER@@", Config.SatelliteNumber)
                    .Replace("@@TERMINALNUMBER@@", Config.TerminalNumber)
                    .Replace("downloaders", "manifestDownloader");

                elementToEncrypt.InnerXml += elementToEncrypt.GetElementsByTagName("manifestDownloader")[0].OuterXml;

                if (elementToEncrypt["manifest"] != null)
                    elementToEncrypt["manifest"].RemoveAll();

                doc.Save(universalUpdaterConfigPath);

                // prepare services.xml
                doc.Load(servicesConfigPath);
                doc.InnerXml = doc.InnerXml.Replace("@@INSTANCEID@@", Config.InstanceID);
                doc.Save(servicesConfigPath);

                logger.Info(m => m("Updater deployed succesfully."));

                // run process
                ExecuteUpdater(20 * 1000);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Alert(new Alerts.AlertError(String.Format("Universal updater extraction fails: {0}", ex.Message)));

                if (Directory.Exists(Config.UpdaterFolder))
                    Directory.Delete(Config.UpdaterFolder, true);

                StartUpdater(Config.UpdaterRetryDeploymentTime);
            }
        }

        private void CheckUpdater()
        {
            if (updaterProcess == null)
            {
                // pass
            }
            else if (updaterProcess.HasExited)
            {
                if (updaterProcess.ExitCode != 0)
                {
                    logger.Error("The process " + updaterProcess.StartInfo.FileName + " returned with error " + updaterProcess.ExitCode.ToString());

                    updaterProcess = null;
                }
            }
            else if (DateTime.Compare(updaterProcess.StartTime, DateTime.Now.Subtract(new TimeSpan(0, 20, 0))) < 0)
            {
                logger.Error(updaterProcess.StartInfo.FileName + " takes longer than 20 minutes and will be killed.");
                updaterProcess.Kill();
            }
        }

        private void ExecuteUpdater(int delay)
        {
            logger.InfoFormat("Updater execution scheduled in {0} sec.", delay / 1000);
            executeUpdaterTimer = new Timer(ExecuteUpdaterProcess);
            executeUpdaterTimer.Change(delay, Timeout.Infinite);
        }

        private void ExecuteUpdaterProcess(object state)
        {
            try
            {
                if (executeUpdaterTimer != null)
                {
                    executeUpdaterTimer.Change(Timeout.Infinite, Timeout.Infinite);
                    executeUpdaterTimer.Dispose();
                    executeUpdaterTimer = null;
                }

                var filePath = Path.Combine(Config.UpdaterFolder, "EZUniversalUpdaterApp.exe");
                var workingDir = Path.GetDirectoryName(filePath);

                if (updaterProcess != null && !updaterProcess.HasExited)
                {
                    logger.Warn("The process " + updaterProcess.StartInfo.FileName + " could not be started. Another one not finished yet!");
                }
                else if (!string.IsNullOrEmpty(workingDir) && !File.Exists(filePath))
                {
                    logger.Warn("The process " + filePath + " couldn't be started. Specified file location doesn't exists.");
                }
                else
                {
                    var proc = new Process
                    {
                        StartInfo =
                        {
                            FileName = filePath,
                            WorkingDirectory = workingDir
                        }
                    };

                    if (!proc.Start())
                    {
                        logger.Error("Could not start " + proc.StartInfo.FileName);
                    }
                    else
                    {
                        logger.Debug(String.Format("Process started {0}", proc.StartInfo.FileName));
                        updaterProcess = proc;
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Alert(new Alerts.AlertError(String.Format("Universal updater run fails: {0}", ex.Message)));
            }
        }
    }
}
