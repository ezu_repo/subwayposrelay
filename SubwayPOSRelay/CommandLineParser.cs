﻿namespace SubwayPOSRelay
{
    using CommandLineHandlers;
    using System;
    using System.Collections.Generic;

    public interface ICommandLineParser
    {
        ICommandLineHandler Parse();
    }

    public class CommandLineParser : ICommandLineParser
    {
        readonly string[] _args;

        readonly List<string> _commands = new List<string> { "-console", "-install", "-uninstall", "-start", "-stop", "-help" };

        public CommandLineParser(string[] args)
        {
            _args = args;
        }

        public ICommandLineHandler Parse()
        {
            var commandLine = String.Join("", _args);

            if (commandLine.Contains("-console"))
            {
                return new StartAsConsoleHandler(_args);
            }

            if (commandLine.Contains("-install"))
            {
                return new InstallServiceHandler();
            }

            if (commandLine.Contains("-uninstall"))
            {
                return new UninstallServiceHandler();
            }

            if (commandLine.Contains("-start"))
            {
                return new StartServiceHandler();
            }

            if (commandLine.Contains("-stop"))
            {
                return new StopServiceHandler();
            }

            if (commandLine.Contains("-help"))
            {
                return new UsageHandler(_commands);
            }

            return new StartAsServiceHandler();
        }
    }
}