﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using Common.Logging;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using SubwayPOSRelay.TerminalData;
using System.IO;
using System.Reflection;

namespace SubwayPOSRelay.Config
{
    public class SubwayPOSRelayConfiguration
    {
        private const string DEFAULT_REGPATH = "SOFTWARE\\EZUniverse Inc.\\SubwayPOSRelay";
        private static readonly ILog logger = LogManager.GetCurrentClassLogger();

        public string InstanceID { get; private set; }
        public string StoreNumber { get; private set; }
        public string ApplicationVersion { get; private set; }
        public string SatelliteNumber { get; private set; }
        public string TerminalNumber { get; private set; }

        public string DebugStoreNumber { get; private set; }
        public string DebugSatelliteNumber { get; private set; }
        public string DebugTerminalNumber { get; private set; }

        public bool UseDebugValues
        {
            get
            {
                return !(String.IsNullOrWhiteSpace(DebugStoreNumber) || String.IsNullOrWhiteSpace(DebugSatelliteNumber) || String.IsNullOrWhiteSpace(DebugTerminalNumber));
            }
        }

        public XmlNode UniversalUpdaterNode { get; private set; }
        public string UpdaterFolder { get; private set; }
        public int UpdaterRetryDeploymentTime { get; private set; }

        //public int[] UpdaterExecuteHours { get; private set; }
        public TimeSpan[] UpdaterExecuteTime { get; private set; }

        private static object threadSync = new object();

        private static SubwayPOSRelayConfiguration _instance;

        private static SubwayPOSRelayConfiguration GetInstance()
        {
            if (_instance != null)
            {
                return _instance;
            }
            else
            {
                lock (threadSync)
                {
                    if (_instance == null)
                    {
                        _instance = new SubwayPOSRelayConfiguration();
                    }
                    _instance.Init();
                    return _instance;
                }
            }
        }

        public static SubwayPOSRelayConfiguration Instance
        {
            get { return GetInstance(); }
        }

        public bool IsServiceSet = false;
        public bool IsTerminalSet = false;

        public bool IsInitiated
        {
            get
            {
                return IsServiceSet && IsTerminalSet;
            }
        }

        public void UpdateTerminalInfo(ITerminalInfo ti)
        {
            StoreNumber = ti.StoreNumber;
            ApplicationVersion = ti.ApplicationVersion;
            SatelliteNumber = ti.SatelliteNumber;
            TerminalNumber = ti.TerminalNumber;

            IsTerminalSet = true;
        }

        private void Init()
        {
            var s = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).GetSection("SubwayPOSRelay");
            XElement x = XElement.Parse(s.SectionInformation.GetRawXml());
            XmlDocument xmlDoc = null;
            if (x != null)
            {
                using (XmlReader xmlReader = x.CreateReader())
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.Load(xmlReader);
                }
            }
            if (xmlDoc != null)
            {
                Load(xmlDoc);
            }
        }

        public void Load(XmlNode node)
        {
            UniversalUpdaterNode = node.SelectSingleNode("//ezUniversalUpdater");
            var subwayPOSRelayNode = node.SelectSingleNode("//SubwayPOSRelay");
            var attr = subwayPOSRelayNode.Attributes["updaterSchedule"];
            if (attr != null)
            {
                List<string> ls = new List<string>(attr.Value.Split(new char[] { ',' }));
                List<TimeSpan> lts = new List<TimeSpan>();

                ls.ForEach(s =>
                    {
                        int i;
                        TimeSpan ts;
                        if( int.TryParse(s, out i))
                        {
                            lts.Add(new TimeSpan(i, 0, 0));
                        }
                        else
                        {
                            if (TimeSpan.TryParse(s, out ts))
                            {
                                lts.Add(ts);
                            }
                        }



                        //int j;
                        //if (int.TryParse(s, out j))
                        //{
                        //    li.Add(j);
                        //}
                    });
                //UpdaterExecuteHours = li.ToArray();
                if (lts.Count == 0)
                {
                    lts.Add(new TimeSpan(0, 19, 0));
                }
                
                UpdaterExecuteTime = lts.ToArray();
            }

            attr = subwayPOSRelayNode.Attributes["debugStoreNumber"];
            DebugStoreNumber = attr != null ? attr.Value : String.Empty;
            attr = subwayPOSRelayNode.Attributes["debugSatelliteNumber"];
            DebugSatelliteNumber = attr != null ? attr.Value : String.Empty;
            attr = subwayPOSRelayNode.Attributes["debugTerminalNumber"];
            DebugTerminalNumber = attr != null ? attr.Value : String.Empty;

            UpdaterRetryDeploymentTime = 900000;
            UpdaterFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\updater";

            InstanceID = GetInstanceID(DEFAULT_REGPATH, "InstanceID");
            IsServiceSet = true;
        }

        private SubwayPOSRelayConfiguration(){}

        private static string GetInstanceID(string registryPath, string valueName)
        {
            try
            {
                string instanceID;
                RegistryKey rk = Registry.LocalMachine.CreateSubKey(registryPath);
                if (rk != null)
                {
                    // key available (created or opened)
                    var value = rk.GetValue(valueName);
                    if (value != null)
                    {
                        // value available, convert to string
                        instanceID = value.ToString();
                    }
                    else
                    {
                        // no value - fill created key with values
                        rk.SetValue("CreatedDate", DateTime.Now);

                        instanceID = Guid.NewGuid().ToString("D");
                        rk.SetValue(valueName, instanceID);
                    }
                    rk.Close();
                    return instanceID;
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to get InstanceID from registry. {0}", ex.Message);
            }
            // return default value
            return String.Format("{0}-{1}", "AABBCCDD", Guid.NewGuid().ToString("D").Substring(9));
        }


    }


}
