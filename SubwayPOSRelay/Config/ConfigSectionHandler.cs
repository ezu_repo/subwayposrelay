﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace SubwayPOSRelay.Config
{
    public class ConfigSectionHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            var config = SubwayPOSRelayConfiguration.Instance;
            if (section != null)
            {
                config.Load(section);
            }
            return config;
        }
    }
}
