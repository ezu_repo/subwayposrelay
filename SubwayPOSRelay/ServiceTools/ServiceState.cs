﻿namespace SubwayPOSRelay.ServiceTools
{
    public enum ServiceState
    {
        Unknown = -1, // The state cannot be (has not been) retrieved.

        NotFound = 0, // The service is not known on the host server.

        Stop = 1, // The service is NET stopped.

        Run = 2, // The service is NET started.

        Stopping = 3,

        Starting = 4,
    }
}