﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using SubwayPOSRelay.TerminalData;

namespace SubwayPOSRelay
{
    class RelayState
    {
        public IMessage CurrentMessage { get; private set; }
        public int QueueLenght { get; private set; }

        public RelayState(IMessage currentMessage, int queueLenght)
        {
            CurrentMessage = currentMessage;
            QueueLenght = queueLenght;
        }
    }
}
