﻿using Common.Logging;
using EZWebserviceWrapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Net.NetworkInformation;
using SubwayPOSRelay.TerminalData;

namespace SubwayPOSRelay
{
    class DCConnector
    {
        private static readonly ILog logger = LogManager.GetCurrentClassLogger();

        //private string[] serviceUris;
        private string serviceUri;
        private string dataUri;

        public DCConnector(string dataUri, string serviceUri)
        {
            if (String.IsNullOrWhiteSpace(dataUri))
            {
                throw new ArgumentException("dataUri cannot be null or empty");
            }

            if ( String.IsNullOrWhiteSpace(serviceUri) )
            {
                throw new ArgumentException("serviceUri cannot be null or empty");
            }

            this.dataUri = dataUri;
            //this.serviceUris = new string[] { serviceUri.Replace("http://", "https://"), serviceUri.Replace("https://", "http://") };
            this.serviceUri = serviceUri;

            //if (serviceUris == null || !serviceUris.Any())
            //    throw new ArgumentException("serviceUris cannot be null or empty");

            //this.serviceUris = serviceUris;
        }

        public IPAddress GetControllerAddress(RelayState state)
        {
            try
            {
                return GetControllerAddress(new POSData(state.CurrentMessage), state.QueueLenght);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return null;
            }
        }

        public IPAddress GetControllerAddress(POSData pos, int queueLenght)
        {
            string storeNumber = pos.StoreNumber;

            if (String.IsNullOrEmpty(storeNumber))
            {
                throw new ArgumentException("StoreNumber cannot be null or empty");
            }

            IPAddress gatewayIP = GetGatewayAddress();
            string terminalNumber = pos.TerminalNumber;
            //XElement terminalData = new XElement("TerminalData",
            //    new XAttribute("StoreNumber", storeNumber == null ? String.Empty: storeNumber),
            //    new XAttribute("SatelliteNumber", pos.SatelliteNumber == null? String.Empty: pos.SatelliteNumber),
            //    new XAttribute("TerminalNumber", pos.TerminalNumber == null? String.Empty: pos.TerminalNumber),
            //    new XAttribute("ApplicationVersion", pos.ApplicationVersion == null? String.Empty: pos.ApplicationVersion),
            //    new XAttribute("DefaultGatewayIP", gatewayIP == null? String.Empty: gatewayIP.ToString()),
            //    new XAttribute("QueueLenght", queueLenght));
            //logger.InfoFormat("Sending information to DataCenter: {0}", terminalData);
            XElement statusData = new XElement("StatusData",
                new XAttribute("ApplicationVersion", pos.ApplicationVersion == null ? String.Empty : pos.ApplicationVersion),
                new XAttribute("DefaultGatewayIP", gatewayIP == null ? String.Empty : gatewayIP.ToString()),
                new XAttribute("QueueLenght", queueLenght));
            logger.InfoFormat("Sending information to DataCenter: {0}", statusData);

            string controllerIp = null;
            //controllerIp = GetControllerIp(serviceUri, storeNumber, terminalData);
            controllerIp = GetControllerIp(serviceUri, storeNumber, pos.SatelliteNumber, pos.TerminalNumber, statusData);

            IPAddress address;
            if (!String.IsNullOrEmpty(controllerIp) && IPAddress.TryParse(controllerIp, out address))
            {
                logger.InfoFormat("Received address: {0}", address.ToString());
                return address;
            }

            logger.Warn("Could not obtain controller address for " + storeNumber);
            return null;
        }

        private string GetControllerIp(string uri, string storeNumber, string satelliteNumber, string terminalNumber, XElement statusData)
        {
            try
            {
                var request = new WSDB2(uri, "EXEC [Data].[SubwayPOSRelay_StatusDataExchange] @StoreNumber, @SatelliteNumber, @TerminalNumber, @InstanceID, @StatusData, @LocalIP OUTPUT", 60, 60);
                request.Parameters.Add(new WSDBParameter("@StoreNumber", storeNumber, SqlDbType.VarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@SatelliteNumber", satelliteNumber, SqlDbType.VarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@TerminalNumber", terminalNumber, SqlDbType.VarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@InstanceID", Config.SubwayPOSRelayConfiguration.Instance.InstanceID, SqlDbType.VarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@StatusData", statusData, SqlDbType.Xml, ParameterDirection.Input));
                request.Parameters.Add(new WSDBParameter("@LocalIP", null, SqlDbType.VarChar, ParameterDirection.Output, 250));

                request.OnError += RequestErr;
                request.OnWarning += RequestReport;
                request.OnInfo += RequestReport;

                var res = request.Execute();
                return (string)request.Parameters["@LocalIP"].Value;
            }
            catch (Exception ex)
            {
                logger.InfoFormat("Could not send network information and receive ip address for {0} from {1}. Details: {2}", storeNumber, uri, ex.Message);
                return null;
            }
        }

        private string GetControllerIp(string uri, string storeNumber, XElement posData)
        {
            try
            {
                var request = new WSDB2(uri, "EXEC [Data].[GetLocalControllerIP] @StoreNumber, @LocalIP OUTPUT, @TerminalData", 60, 60);
                request.Parameters.Add(new WSDBParameter("@StoreNumber", storeNumber, SqlDbType.VarChar, ParameterDirection.Input, 250));
                request.Parameters.Add(new WSDBParameter("@LocalIP", null, SqlDbType.VarChar, ParameterDirection.Output, 250));
                request.Parameters.Add(new WSDBParameter("@TerminalData", posData, SqlDbType.Xml, ParameterDirection.Input));

                request.OnError += RequestErr;
                request.OnWarning += RequestReport;
                request.OnInfo += RequestReport;

                var res = request.Execute();
                return (string)request.Parameters["@LocalIP"].Value;
            }
            catch (Exception ex)
            {
                logger.InfoFormat("Could not send network information and receive ip address for {0} from {1}. Details: {2}", storeNumber, uri, ex.Message);
                return null;
            }
        }

        private void RequestReport(string message)
        {
            logger.Debug(message);
        }

        private void RequestErr(string message, Exception exception)
        {
            if (!String.IsNullOrEmpty(message))
            {
                logger.Error(message);
            }

            if (exception != null)
            {
                logger.Error(exception);
            }
        }

        private IPAddress GetGatewayAddress()
        {
            logger.Debug("Getting default gateway address");
            try
            {
                var gateway = NetworkInterface.GetAllNetworkInterfaces().Where(e => e.OperationalStatus == OperationalStatus.Up).SelectMany(e => e.GetIPProperties().GatewayAddresses).FirstOrDefault();
                if (gateway == null)
                {
                    logger.Info("No gateway address in available interfaces");
                    return null;
                }
                else
                {
                    return gateway.Address;
                }
            }
            catch (Exception ex)
            {
                logger.Error(String.Format("Unable to get default gateway IP. {0}", ex.Message));
            }
            return null;
        }

        public bool SendMessage(IMessage message)
        {
            try
            {
                var tda = new TerminalDataAdapter(message);
                var request = new WSDB2(dataUri, "EXEC [Buffer].[AddSubwayPOSMessage] @StoreNumber, @SatelliteNumber, @TerminalNumber, @MessageID, @InstanceID, @Data", 60, 60);
                request.Parameters.Add(new WSDBParameter("@StoreNumber", tda.Terminal.StoreNumber, SqlDbType.NVarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@SatelliteNumber", tda.Terminal.SatelliteNumber, SqlDbType.NVarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@TerminalNumber", tda.Terminal.TerminalNumber, SqlDbType.NVarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@MessageID", tda.MessageId, SqlDbType.NVarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@InstanceID", Config.SubwayPOSRelayConfiguration.Instance.InstanceID, SqlDbType.NVarChar, ParameterDirection.Input, 50));
                request.Parameters.Add(new WSDBParameter("@Data", tda.Data.OuterXml, SqlDbType.Xml, ParameterDirection.Input));

                request.OnError += RequestErr;
                request.OnWarning += RequestReport;
                request.OnInfo += RequestReport;

                var res = request.Execute();
                return true;
            }
            catch (Exception ex)
            {
                logger.InfoFormat("Could not send message to {0}. Details: {1}", dataUri, ex.Message);
                return false;
            }
        }
    }
}
