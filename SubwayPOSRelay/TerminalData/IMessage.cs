﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace SubwayPOSRelay.TerminalData
{
    public interface IMessage
    {
        byte[] Bytes { get; }
        IPEndPoint Sender { get; }
    }
}
