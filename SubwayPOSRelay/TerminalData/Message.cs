﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace SubwayPOSRelay.TerminalData
{
    class Message: IMessage
    {
        public byte[] Bytes { get; private set; }
        public IPEndPoint Sender { get; private set; }

        public Message(byte[] bytes, IPEndPoint sender)
        {
            Bytes = bytes;
            Sender = sender;
        }
    }
}
