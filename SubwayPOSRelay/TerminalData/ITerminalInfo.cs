﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SubwayPOSRelay.TerminalData
{
    public interface ITerminalInfo
    {
        string StoreNumber { get; set; }
        string ApplicationVersion { get; set; }
        string SatelliteNumber { get; set; }
        string TerminalNumber { get; set; }
    }
}
