﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SubwayPOSRelay.TerminalData
{
    public class TerminalDataAdapter
    {
        public ITerminalInfo Terminal { get; private set; }
        public string MessageId { get; private set; }
        public XmlNode Data { get; private set; }

        public TerminalDataAdapter(IMessage message)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(Encoding.ASCII.GetString(message.Bytes));

            Terminal = new TerminalInfo()
            {
                StoreNumber = GetAttribute(xml.DocumentElement, "StoreNumber"),
                SatelliteNumber = GetAttribute(xml.DocumentElement, "SatelliteNumber"),
                TerminalNumber = GetAttribute(xml.DocumentElement, "TerminalNumber"),
                ApplicationVersion = GetAttribute(xml.DocumentElement, "ApplicationVersion")
            };
            MessageId = GetAttribute(xml.DocumentElement, "MessageId");
            Data = xml;
        }

        private string GetAttribute(XmlElement root, string attrName)
        {
            var node = root.SelectSingleNode(String.Format("//*[@{0}]", attrName));
            if (node == null)
            {
                throw new ArgumentException(String.Format("Could not parse message for '{1}'", attrName));
            }
            return node.Attributes[attrName].Value;
        }
    }
}
