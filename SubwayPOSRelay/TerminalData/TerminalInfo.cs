﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SubwayPOSRelay.TerminalData
{
    class TerminalInfo: ITerminalInfo
    {
        public string StoreNumber { get; set; }
        public string ApplicationVersion { get; set; }
        public string SatelliteNumber { get; set; }
        public string TerminalNumber { get; set; }
    }
}
