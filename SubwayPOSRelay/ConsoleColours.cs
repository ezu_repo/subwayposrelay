﻿namespace SubwayPOSRelay
{
    using System;

    public class ConsoleColours
    {
        public delegate void Action();

        public static void Background(ConsoleColor colour, Action action)
        {
            var original = Console.BackgroundColor;
            Console.BackgroundColor = colour;

            action();

            Console.BackgroundColor = original;
        }

        public static void Foreground(ConsoleColor colour, Action action)
        {
            var original = Console.ForegroundColor;
            Console.ForegroundColor = colour;

            action();

            Console.ForegroundColor = original;
        }
    }
}