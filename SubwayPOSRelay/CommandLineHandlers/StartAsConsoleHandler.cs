﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using System;

    public class StartAsConsoleHandler : ICommandLineHandler
    {
        readonly string[] _args;

        public StartAsConsoleHandler(string[] args)
        {
            _args = args;
        }

        public void Execute()
        {
            var service = new SubwayPOSRelay();
            service.StartService(_args);

            Console.TreatControlCAsInput = true;

            ConsoleColours.Foreground(ConsoleColor.Gray, () => Console.WriteLine(@"Press CTRL+C to quit."));
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.C && keyInfo.Modifiers == ConsoleModifiers.Control)
                {
                    break;
                }
            }

            service.Stop();
        }
    }
}