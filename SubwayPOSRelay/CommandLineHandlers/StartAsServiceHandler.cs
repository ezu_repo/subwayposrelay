﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using System.ServiceProcess;

    public class StartAsServiceHandler : ICommandLineHandler
    {
        public void Execute()
        {
            var servicesToRun = new ServiceBase[] { new SubwayPOSRelay(), };
            ServiceBase.Run(servicesToRun);
        }
    }
}