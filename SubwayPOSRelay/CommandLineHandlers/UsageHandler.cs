﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public class UsageHandler : ICommandLineHandler
    {
        readonly IEnumerable<string> _commands;

        public UsageHandler(IEnumerable<string> commands)
        {
            _commands = commands;
        }

        public void Execute()
        {
            var exeName = Assembly.GetExecutingAssembly().ManifestModule.Name;
            Console.WriteLine(@"Usage:");
            foreach (var item in _commands)
            {
                Console.WriteLine(@"  " + exeName + @" " + item);
            }

            Console.Read();
        }
    }
}