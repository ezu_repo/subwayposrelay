﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    public interface ICommandLineHandler
    {
        void Execute();
    }
}