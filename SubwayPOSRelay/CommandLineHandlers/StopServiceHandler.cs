﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using ServiceTools;

    public class StopServiceHandler : ICommandLineHandler
    {
        public void Execute()
        {
            ServiceInstaller.StopService("SubwayPOSRelay");
        }
    }
}