﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using ServiceTools;

    public class StartServiceHandler : ICommandLineHandler
    {
        public void Execute()
        {
            ServiceInstaller.StartService("SubwayPOSRelay");
        }
    }
}