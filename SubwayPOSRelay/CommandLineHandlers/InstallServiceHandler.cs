﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using System.Reflection;
    using ServiceTools;

    public class InstallServiceHandler : ICommandLineHandler
    {
        public void Execute()
        {
            ServiceInstaller.InstallAndStart("SubwayPOSRelay", "SubwayPOSRelay", Assembly.GetExecutingAssembly().Location);
        }
    }
}