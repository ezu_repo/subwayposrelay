﻿namespace SubwayPOSRelay.CommandLineHandlers
{
    using ServiceTools;

    public class UninstallServiceHandler : ICommandLineHandler
    {
        public void Execute()
        {
            ServiceInstaller.Uninstall("SubwayPOSRelay");
        }
    }
}