﻿namespace SubwayPOSRelay
{
    using System;
    using System.Reflection;

    using Alerts;

    using global::Common.Logging;
    using log4net.Core;

    public static class CommonLoggingExtensions
    {
        public static void Alert(this ILog log, Alert alert)
        {
            try
            {
                Type logType = log.GetType();

                FieldInfo fieldInfo = logType.GetField("_logger",
                        BindingFlags.Instance |
                        BindingFlags.NonPublic);
                if (fieldInfo != null)
                {
                    var logger = (ILogger)fieldInfo.GetValue(log);

                    if (logger != null)
                    {
                        logger.Log(MethodBase.GetCurrentMethod().DeclaringType,
                                Level.Alert, alert.ToString(), null);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Internal logging error: {0}", ex);
            }
        }
    }
}
