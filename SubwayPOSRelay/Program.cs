﻿namespace SubwayPOSRelay
{
    using System.ServiceProcess;

    public static class Program
    {
        public static void Main(string[] args)
        {
            var parser = new CommandLineParser(args);
            var handler = parser.Parse();
            handler.Execute();
        }
    }
}