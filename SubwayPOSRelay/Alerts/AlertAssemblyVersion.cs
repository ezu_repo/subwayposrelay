﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayPOSRelay.Alerts
{
    public class AlertAssemblyVersion : Alert
    {
        public AlertAssemblyVersion(string version)
        {
            _props.Add("code", "A502");
            _props.Add("version", version);
        }
    }
}
