﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SubwayPOSRelay.Alerts
{
    public class AlertError : Alert
    {
        public AlertError(string text)
        {
            _props.Add("code", "A007");
            text = text.Replace("&", "&amp;");
            text = text.Replace("'", "&apos;");
            text = text.Replace("\"", "&quot;");
            text = text.Replace("<", "&lt;");
            text = text.Replace(">", "&gt;");
            _props.Add("text", text);
        }
    }
}
