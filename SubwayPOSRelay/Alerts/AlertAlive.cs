﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SubwayPOSRelay.Alerts
{
    public class AlertAlive : Alert
    {
        public AlertAlive()
        {
            _props.Add("code", "A510");
        }
    }
}
