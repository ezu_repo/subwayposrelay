﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubwayPOSRelay.Alerts
{
    public class AlertVersion : Alert
    {
        public AlertVersion(string version)
        {
            _props.Add("code", "A501");
            _props.Add("version", version);
        }
    }
}
