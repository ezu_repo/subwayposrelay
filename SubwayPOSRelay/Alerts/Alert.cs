﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubwayPOSRelay.Config;

namespace SubwayPOSRelay.Alerts
{
    public abstract class Alert
    {
        /// <summary>
        ///<log>
        ///     <alert code="A001" class="AlertAlive" id="2DA40239-4D31-4D67-BFEB-FC19F4CD1A98" />
        ///</log>
        /// </summary>
        /// 
        protected Hashtable _props = new Hashtable();

        public Alert()
        {
            _props.Add("InstanceID", DefaultValueOnEmpty(SubwayPOSRelayConfiguration.Instance.InstanceID));
            _props.Add("StoreNumber", DefaultValueOnEmpty(SubwayPOSRelayConfiguration.Instance.StoreNumber));
            _props.Add("SatelliteNumber", DefaultValueOnEmpty(SubwayPOSRelayConfiguration.Instance.SatelliteNumber));
            _props.Add("TerminalNumber", DefaultValueOnEmpty(SubwayPOSRelayConfiguration.Instance.TerminalNumber));
            _props.Add("ApplicationVersion", DefaultValueOnEmpty(SubwayPOSRelayConfiguration.Instance.ApplicationVersion));
            _props.Add("class", GetType().Name);
        }

        public void AddProp(object key, object value)
        {
            if (!_props.ContainsKey(key))
            {
                _props.Add(key, value);
            }
            else
            {
                _props[key] = value;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<log>");
            sb.Append("<alert ");

            foreach (string key in _props.Keys)
            {
                sb.Append(key);
                sb.Append("=\"");
                sb.Append(_props[key]);
                sb.Append("\" ");
            }
            sb.Append("/>");
            sb.Append("</log>");
            return sb.ToString();
        }

        private string DefaultValueOnEmpty(string value)
        {
            return DefaultValueOnEmpty(value, "Unknown");
        }

        private string DefaultValueOnEmpty(string value, string defaultValue)
        {
            return String.IsNullOrWhiteSpace(value) ? defaultValue : value;
        }
    }
}
