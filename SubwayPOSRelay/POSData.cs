﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Xml;

namespace SubwayPOSRelay
{
    using TerminalData;
    class POSData
    {
        public enum SpecialMessageType { Startup, SystemShutdown };

        private const string ALIVE_TEMPLATE =
"<SubwayPOSTransaction xmlns=\"SubwayPOSDataAPI\">" +
    "<RelayAlive Version=\"1.0\" ApplicationVersion=\"{0}\" MessageId=\"{1}\" StoreNumber=\"{2}\" SatelliteNumber=\"{3}\" TerminalNumber=\"{4}\" TransactionDate=\"{5:yyyy-MM-ddTHH:mm:ss.fff}\" " +
                "IPAddress=\"{6}\" MACAddress=\"{7}\" TimeZone=\"{8}\" />" +
"</SubwayPOSTransaction>";

        private const string STARTUP_TEMPLATE =
"<SubwayPOSTransaction xmlns=\"SubwayPOSDataAPI\">" +
"<RelayStartUp Version=\"1.0\" ApplicationVersion=\"{0}\" MessageId=\"{1}\" StoreNumber=\"{2}\" SatelliteNumber=\"{3}\" TerminalNumber=\"{4}\" TransactionDate=\"{5:yyyy-MM-ddTHH:mm:ss.fff}\" " +
        "IPAddress=\"{6}\" MACAddress=\"{7}\" TimeZone=\"{8}\">{9}</RelayStartUp>" +
"</SubwayPOSTransaction>";

        private const string SYSTEMSHUTDOWN_TEMPLATE =
"<SubwayPOSTransaction xmlns=\"SubwayPOSDataAPI\">" +
"<RelaySystemShutdown Version=\"1.0\" ApplicationVersion=\"{0}\" MessageId=\"{1}\" StoreNumber=\"{2}\" SatelliteNumber=\"{3}\" TerminalNumber=\"{4}\" TransactionDate=\"{5:yyyy-MM-ddTHH:mm:ss.fff}\" " +
        "IPAddress=\"{6}\" MACAddress=\"{7}\" TimeZone=\"{8}\">{9}</RelaySystemShutdown>" +
"</SubwayPOSTransaction>";

        private static readonly ILog logger = LogManager.GetCurrentClassLogger();
        private static readonly byte[] subwayPOSTransactionRoot = Encoding.ASCII.GetBytes("<SubwayPOSTransaction");

        public IPAddress Address { get; private set; }
        public string StoreNumber { get; private set; }
        public string ApplicationVersion { get; private set; }
        public string SatelliteNumber { get; private set; }
        public string TerminalNumber { get; private set; }
        public string MessageId { get; private set; }

        public POSData(IMessage message)
        {
            if (message != null)
            {
                Address = message.Sender.Address;
                try
                {
                    var tda = new TerminalDataAdapter(message);

                    StoreNumber = tda.Terminal.StoreNumber;
                    ApplicationVersion = tda.Terminal.ApplicationVersion;
                    SatelliteNumber = tda.Terminal.SatelliteNumber;
                    TerminalNumber = tda.Terminal.TerminalNumber;
                    MessageId = tda.MessageId;
                }
                catch (Exception ex)
                {
                    throw new ArgumentException(String.Format("Error in parsing message from address '{0}'; {1}", Address, ex.Message));
                }
            }
        }

        public static bool IsValidMessage(byte[] message, IPEndPoint sender)
        {
            if (message.Length < subwayPOSTransactionRoot.Length)
            {
                return false;
            }

            for (int i = 0; i < subwayPOSTransactionRoot.Length; ++i)
            {
                if (message[i] != subwayPOSTransactionRoot[i])
                {
                    return false;
                }
            }

            try
            {
                System.Xml.Linq.XDocument.Parse(Encoding.ASCII.GetString(message));
            }
            catch(Exception)
            {
                // not valid xml
                return false;
            }

            return true;
        }

        public string GetSpecialMessage(IPEndPoint controllerIP, string description, SpecialMessageType messageType)
        {
            NetworkInterface nic;
            IPAddress address;
            bool nicFound = GetNIC(controllerIP, out nic, out address);
            string template = "";
            switch (messageType)
            {
                case SpecialMessageType.Startup:
                    template = STARTUP_TEMPLATE;
                    break;
                case SpecialMessageType.SystemShutdown:
                    template = SYSTEMSHUTDOWN_TEMPLATE;
                    break;
                default:
                    break;
            }
            return String.Format(template, ApplicationVersion, Guid.NewGuid(), StoreNumber, SatelliteNumber, TerminalNumber, DateTime.Now,
                                     nicFound ? address : null, nicFound ? nic.GetPhysicalAddress() : null, TimeZoneInfo.Local.BaseUtcOffset, description);

        }

        public string GetAliveMessage(IPEndPoint controllerIP)
        {
            NetworkInterface nic;
            IPAddress address;
            bool nicFound = GetNIC(controllerIP, out nic, out address);
            return String.Format(ALIVE_TEMPLATE, ApplicationVersion, Guid.NewGuid(), StoreNumber, SatelliteNumber, TerminalNumber, DateTime.Now,
                                                 nicFound ? address : null, nicFound ? nic.GetPhysicalAddress() : null, TimeZoneInfo.Local.BaseUtcOffset);
        }

        private bool GetNIC(IPEndPoint controllerIP, out NetworkInterface resultNIC, out IPAddress address)
        {
            resultNIC = null;
            address = null;

            if (controllerIP != null)
            {
                foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    var addresses = nic.GetIPProperties().UnicastAddresses.Where(q => q.Address.AddressFamily == AddressFamily.InterNetwork).ToList();
                    if (addresses.Any())
                    {
                        var addressInfo = addresses.FirstOrDefault(q => IsInSubnet(q.Address, controllerIP.Address, q.IPv4Mask));
                        if (addressInfo != null)
                        {
                            resultNIC = nic;
                            address = addressInfo.Address;
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsInSubnet(IPAddress a1, IPAddress a2, IPAddress mask)
        {
            if (a1 == null || a2 == null || mask == null)
            {
                return false;
            }

            var a1bytes = a1.GetAddressBytes();
            var a2bytes = a2.GetAddressBytes();
            var maskBytes = mask.GetAddressBytes();

            if (a1bytes.Length < maskBytes.Length || a2bytes.Length < maskBytes.Length)
            {
                return false;   //addresses are not long enough to fit in mask (is it possible?)
            }

            for (int i = 0; i < maskBytes.Length; ++i)
            {
                if ((a1bytes[i] & maskBytes[i]) != (a2bytes[i] & maskBytes[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
