﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace EZProtocolTranslator.Listener
{
    public class UdpListener : Listener
    {
        public override ProtocolType ProtocolType { get { return ProtocolType.Udp; } }

        public UdpListener(int port)
            : base(port)
        {
            sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        }
    }
}
