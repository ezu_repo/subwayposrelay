﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace EZProtocolTranslator.Listener
{
    class ReceiveState
    {
        public const int BUFF_SIZE = 512 * 1024;

        public Socket Socket { get; private set; }
        public byte[] Buffer { get; private set; }

        public ReceiveState(Socket sock)
        {
            Socket = sock;
            Buffer = new byte[BUFF_SIZE];
        }
    }
}
