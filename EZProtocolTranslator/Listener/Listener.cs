﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace EZProtocolTranslator.Listener
{
    public abstract class Listener : IDisposable
    {
        protected static readonly ILog logger = LogManager.GetCurrentClassLogger();

        public delegate void PacketReceivedDelegate(byte[] message, IPEndPoint sender);

        protected Socket sock;
        protected bool disposed = false;

        public event PacketReceivedDelegate PacketReceived;

        public int Port { get; private set; }
        public abstract ProtocolType ProtocolType { get; }

        public Listener(int port)
        {
            Port = port;
        }
        ~Listener()
        {
            Dispose(false);
        }

        public virtual void StartListening()
        {
            if (sock == null)
                throw new InvalidOperationException("Socket has not been initialized");

            try
            {
                sock.Bind(new IPEndPoint(IPAddress.Any, Port));
                sock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.PacketInformation, true);
                BeginReceive();

                logger.Info("Started listening on port " + Port);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Could not start listening on port {0}. Details: {1}", Port, ex.Message);
                throw;
            }
        }

        protected virtual void BeginReceive()
        {
            try
            {
                var rs = new ReceiveState(sock);
                var dummy = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
                sock.BeginReceiveFrom(rs.Buffer, 0, rs.Buffer.Length, SocketFlags.None, ref dummy, OnPacketReceived, rs);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected virtual void OnPacketReceived(IAsyncResult ar)
        {
            var rs = ar.AsyncState as ReceiveState;
            if (rs == null || disposed)
                return;

            try
            {
                var ep = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
                int bytes = rs.Socket.EndReceiveFrom(ar, ref ep);

                if (bytes > 0)
                {
                    var msg = new byte[bytes];
                    Array.Copy(rs.Buffer, msg, msg.Length);

                    BeginReceive();

                    logger.InfoFormat("Message from: '{0}'. Length: {1}", ep, msg.Length);
                    if (logger.IsTraceEnabled)
                        logger.Trace("Received message: " + Encoding.ASCII.GetString(msg));

                    var handler = PacketReceived;
                    if (handler != null)
                        handler(msg, (IPEndPoint)ep);
                }
                else
                {
                    logger.WarnFormat("Failed reading data from {0}", ep);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            disposed = true;

            sock.Close();
            sock.Dispose();
            sock = null;
        }
    }
}
