﻿using Common.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace EZProtocolTranslator.Sender
{
    public abstract class Sender : IDisposable
    {
        protected static readonly ILog logger = LogManager.GetCurrentClassLogger();

        protected readonly object sockSyncRoot = new object();
        protected Socket sock;
        protected IPEndPoint target;

        public event EventHandler Connected;
        public event EventHandler Disconnected;

        public virtual bool IsConnected { get { return sock != null; } }

        ~Sender()
        {
            Dispose(false);
        }

        protected virtual void OnConnected()
        {
            var handler = Connected;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        protected virtual void OnDisconnected()
        {
            var handler = Disconnected;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        public virtual bool Connect(IPEndPoint target)
        {
            logger.InfoFormat("Connecting to \"{0}\"", target);
            this.target = target;
            return true;
        }

        public virtual void Disconnect()
        {
            lock (sockSyncRoot)
            {
                if (sock != null)
                {
                    if (sock.Connected)
                    {
                        logger.InfoFormat("Disconnecting from \"{0}\"", sock.RemoteEndPoint);
                    }
                    sock.Close();
                    sock = null;
                }
            }
        }

        public virtual bool Send(byte[] message)
        {
            lock (sockSyncRoot)
            {
                if (sock == null)
                    throw new InvalidOperationException("Socket has not been initialized");

                logger.InfoFormat("Sending message to: '{0}'. Length: {1}", target, message.Length);
                if (logger.IsTraceEnabled)
                    logger.TraceFormat("Sending message: \"{0}\"", Encoding.ASCII.GetString(message));

                try
                {
                    int totalSent = 0;
                    while (totalSent < message.Length)
                    {
                        int sent = sock.Send(message, totalSent, message.Length - totalSent, SocketFlags.None);
                        if (sent == 0)
                        {
                            if (!IsConnected)
                                OnDisconnected();
                            break;
                        }
                        totalSent += sent;
                    }

                    return totalSent >= message.Length;
                }
                catch (SocketException sex)
                {
                    logger.WarnFormat("Sending message failed: {0}", sex.Message);
                    //if (!IsConnected)
                        OnDisconnected();
                    return false;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            Disconnect();
        }

        public bool ConnectAndSend(IPEndPoint target, byte[] message)
        {
            bool sent = false;
            if (_InitConnection(target))
            {
                sent = _SendMessage(message);
            }
            _FinishConnection();
            return sent;
        }

        protected abstract bool _InitConnection(IPEndPoint target);
        protected abstract bool _SendMessage(byte[] message);
        protected abstract bool _FinishConnection();

    }
}
