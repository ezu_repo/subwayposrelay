﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace EZProtocolTranslator.Sender
{
    public class TcpSender : Sender
    {
        private const int CONNECT_TIMEOUT = 5000;

        private bool connectionEstablished = false;
        private readonly ManualResetEvent connectingFinished = new ManualResetEvent(false);

        public override bool IsConnected
        {
            get
            {
                lock (sockSyncRoot)
                {
                    if (sock == null || target == null)
                    {
                        return false;
                    }
                    else
                    {
                        return RefreshConnectionState();
                    }
                    //if (RefreshConnectionState())
                    //    return true;

//                    bool reconnected = Connect(target);
                    //if (!reconnected)
                    //    OnDisconnected();
                    //return reconnected;
                }
            }
        }

        public override bool Connect(IPEndPoint target)
        {
            if (target == null)
            {
                return false;
            }

            lock (sockSyncRoot)
            {
                if (RefreshConnectionState())
                {
                    if (target.Address == this.target.Address)
                        return true;

                    Disconnect();
                }

                base.Connect(target);

                try
                {
                    if (sock != null)   //sock is not null, but not connected either
                        sock.Dispose();

                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    connectionEstablished = false;

                    sock.BeginConnect(target, EndConnectCallback, sock);
                    if( connectingFinished.WaitOne(CONNECT_TIMEOUT) )
                    {
                        if (connectionEstablished)
                        {
                            OnConnected();
                            logger.InfoFormat("Connected successfully");
                            return true;
                        }
                        else
                        {
                            logger.InfoFormat("Connection cannot be established to target {0}", target);
                        }
                    }
                    else
                    {
                        logger.WarnFormat("Connection to {0} timed out", target);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("Connection failed: {0}", ex.Message);
                }

                if (IsConnected)
                {
                    Disconnect();
                }
                return false;
            }
        }

        private void EndConnectCallback(IAsyncResult ar)
        {
            logger.Debug("End connection begin");
            var sock = (Socket)ar.AsyncState;
            try
            {
                sock.EndConnect(ar);
                connectionEstablished = true;
                connectingFinished.Set();
                logger.Debug("Connection Signal send");
            }
            catch (Exception ex)    //in case of race condition with dispose
            {
                logger.Debug(ex);
                connectionEstablished = false;
            }
        }

        private byte[] GetKeepAliveValues(uint keepaliveTime, uint keepaliveInterval)
        {
            ////Argument structure for SIO_KEEPALIVE_VALS
            //struct tcp_keepalive {
            //    u_long  onoff;                //If the onoff member is set to a nonzero value, TCP keep-alive is enabled and the other members in the structure are used
            //    u_long  keepalivetime;        //The keepalivetime member specifies the timeout, in milliseconds, with no activity until the first keep-alive packet is sent
            //    u_long  keepaliveinterval;    //The keepaliveinterval member specifies the interval, in milliseconds, between when successive keep-alive packets are sent if no acknowledgement is received
            //};

            int marshaledSizeofUint = Marshal.SizeOf(typeof(uint));
            byte[] inOptionValues = new byte[marshaledSizeofUint * 3];

		    BitConverter.GetBytes(keepaliveTime > 0 ? 1U : 0U).CopyTo(inOptionValues, 0);
            BitConverter.GetBytes(keepaliveTime).CopyTo(inOptionValues, marshaledSizeofUint);
            BitConverter.GetBytes(keepaliveInterval).CopyTo(inOptionValues, marshaledSizeofUint * 2);

            return inOptionValues;
        }

        public override void Disconnect()
        {
            lock (sockSyncRoot)
            {
                bool wasConnected = IsConnected;
                if (wasConnected)
                {
                    sock.Shutdown(SocketShutdown.Both);
                    sock.Disconnect(false);
                }

                base.Disconnect();
                OnDisconnected();
            }
        }

        private bool RefreshConnectionState()
        {
            if (sock == null)
            {
                return false;
            }
            else
            {
                bool part1 = sock.Poll(1000, SelectMode.SelectRead);
                bool part2 = sock.Poll(1000, SelectMode.SelectWrite);
                bool part3 = sock.Poll(1000, SelectMode.SelectError);
                bool part4 = (sock.Available == 0);

                if (!sock.Connected)
                {
                    return false;
                }
                bool blocking = sock.Blocking;
                try
                {
                    sock.Blocking = true;
                    int sent = sock.Send(new byte[128 * 1024], 128 * 1024, SocketFlags.None);
                }
                catch (SocketException sex)
                {
                    if (sex.SocketErrorCode != SocketError.WouldBlock)
                    {
                        return false;
                    }
                }
                finally
                {
                    sock.Blocking = blocking;
                }

                return sock.Connected;
            }
        }

        public override bool Send(byte[] message)
        {
            lock (sockSyncRoot)
            {
                if (!IsConnected)
                {
                    if (target == null)
                        throw new InvalidOperationException("Connection needs to be established first");

                    return false;
                }

                return base.Send(message);
            }
        }

        protected override bool _InitConnection(IPEndPoint target)
        {
            if (target == null)
            {
                return false;
            }

            lock (sockSyncRoot)
            {
                try
                {
                    if (sock != null)   //sock is not null, release previous connection
                    {
                        if (sock.Connected)
                        {
                            sock.Disconnect(false);
                        }
                        sock.Dispose();
                    }

                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    connectionEstablished = false;

                    connectingFinished.Reset();
                    sock.BeginConnect(target, EndConnectCallback, sock);
                    logger.Debug("Begin connecting");
                    if (connectingFinished.WaitOne(CONNECT_TIMEOUT))
                    {
                        logger.Debug("Connection Signal received");
                        if (connectionEstablished)
                        {
                            logger.InfoFormat("Connected successfully to target {0}", target);
                            return true;
                        }
                        else
                        {
                            logger.InfoFormat("Connection cannot be established to target {0}", target);
                        }
                    }
                    else
                    {
                        logger.WarnFormat("Connection to {0} timed out", target);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("Connection failed: {0}", ex.Message);
                }

                return false;
            }

        }

        protected override bool _SendMessage(byte[] message)
        {
            lock (sockSyncRoot)
            {
                if (sock == null || !sock.Connected)
                {
                    logger.InfoFormat("Not connected yet");
                    return false;
                }
                else
                {
                    logger.InfoFormat("Sending message to: '{0}'. Length: {1}", sock.RemoteEndPoint, message.Length);

                    if (logger.IsTraceEnabled)
                    {
                        logger.TraceFormat("Sending message: \"{0}\"", Encoding.ASCII.GetString(message));
                    }

                    try
                    {
                        int totalSent = 0;
                        while (totalSent < message.Length)
                        {
                            int sent = sock.Send(message, totalSent, message.Length - totalSent, SocketFlags.None);
                            if (sent == 0)
                            {
                                break;
                            }
                            totalSent += sent;
                        }

                        return totalSent >= message.Length;
                    }
                    catch (SocketException sex)
                    {
                        logger.WarnFormat("Sending message failed: {0}", sex.Message);
                        return false;
                    }

                }
            }
        }

        protected override bool _FinishConnection()
        {
            // socket not exits, nothing to finish
            if (sock == null)
            {
                return true;
            }

            lock (sockSyncRoot)
            {
                if (sock.Connected)
                {
                    sock.Shutdown(SocketShutdown.Both);
                    sock.Close();
                }
                sock.Dispose();
                sock = null;
                return true;
            }
        }
    }
}
