﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace EZProtocolTranslator.Sender
{
    public class TcpClientSender : Sender
    {
        private readonly object syncRoot = new object();

        private TcpClient client;
        private BufferedStream clientStream;

        public override bool IsConnected
        {
            get
            {
                lock (sockSyncRoot)
                {
                    if (client == null || target == null)
                        return false;

                    if (RefreshConnectionState())
                        return true;

                    bool reconnected = Connect(target);
                    if (!reconnected)
                        OnDisconnected();

                    return reconnected;
                }
            }
        }

        private bool RefreshConnectionState()
        {
            if (client == null || clientStream == null || !client.Connected)
                return false;

            bool blocking = client.Client.Blocking;
            try
            {
                clientStream.Write(new byte[1], 0, 0);
                clientStream.Flush();
            }
            catch (SocketException sex)
            {
                if (sex.SocketErrorCode != SocketError.WouldBlock)
                    return false;
            }
            finally
            {
                client.Client.Blocking = blocking;
            }

            return client.Connected;
        }

        public override bool Connect(IPEndPoint target)
        {
            lock (sockSyncRoot)
            {
                if (RefreshConnectionState())
                {
                    if (target.Address == this.target.Address)
                        return true;

                    Disconnect();
                }

                base.Connect(target);

                try
                {
                    if (clientStream != null)
                        clientStream.Dispose();
                    if (client != null)   //sock is not null, but not connected either
                        client.Close();

                    client = new TcpClient();
                    client.Connect(target);

                    clientStream = new BufferedStream(client.GetStream(), 32 * 1024);

                    logger.InfoFormat("Connected successfully", target);
                    return true;
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("Connection failed: {0}", ex.Message);
                }

                Disconnect();
                return false;
            }
        }

        public override void Disconnect()
        {
            lock (sockSyncRoot)
            {
                if (clientStream != null)
                {
                    clientStream.Dispose();
                    clientStream = null;
                }
                if (client != null)
                {
                    client.Close();
                    client = null;
                }

                base.Disconnect();

                OnDisconnected();
            }
        }

        public override bool Send(byte[] message)
        {
            lock (sockSyncRoot)
            {
                if (!IsConnected)
                {
                    if (target == null)
                        throw new InvalidOperationException("Connection needs to be established first");

                    return false;
                }

                logger.InfoFormat("Sending message to: '{0}'. Length: {1}", target, message.Length);
                if (logger.IsTraceEnabled)
                    logger.TraceFormat("Sending message: \"{0}\"", Encoding.ASCII.GetString(message));

                try
                {
                    clientStream.Write(message, 0, message.Length);
                    clientStream.Flush();
                    return true;
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("Sending message failed: {0}", ex.Message);
                    return false;
                }
            }
        }

        protected override bool _InitConnection(IPEndPoint target)
        {
            throw new NotImplementedException();
        }

        protected override bool _SendMessage(byte[] message)
        {
            throw new NotImplementedException();
        }

        protected override bool _FinishConnection()
        {
            throw new NotImplementedException();
        }
    }
}
